# Life-size 3D Holograms - Live control software

The goal of this project was to investigate current hologram technology, with a focus on the size of the displayed image.
It was determined that building a small controller software was necessary, as it would allow us to easily adapt the projected image live during the exhibit, in order to facilitate setting the booth up.
The scripts attached to either the 3D model or the cameras allow the user to change: 

* The position of the model, using the WASD keys or the arrow keys.
* The rotation of the model, using the Y and C keys. This rotates the model along the Y axis (vertical).
* The scale of the model, using que Q and E keys.
* The pitch of the camera, using the U and J keys.
* The height of the camera, using the Z and H keys.
* The lighting, on or off, using the V key.
* The intensity of the light, using the O and P keys.
* The RGB values of the light, using the R, G and B keys.

Or Reset the lighting to its default values, using the I key. The RBG filter will be reset to 0 and the intensity of the light will be set to 1.

We can let the rabbit run free by pressing the Spacebar, and then simulate an interaction by pressing 1 on the keyboard.
The user can quit the app at any point by holding Escape down.

This project contains 
* the model of the rabbit, with its animations and Animation Controller
* the scripts (including the Kinect Manager script)
* the full scene containing the cameras around the model and the rendering plane