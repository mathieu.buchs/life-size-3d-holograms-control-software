using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
    public float timeSpan = 0.5f;
    private float time;
     
    // Start is called before the first frame update
    void Start()
    {
        time = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.Escape)){
            time += Time.deltaTime;
            //quit the app if we've pressed the escape button long enough
             if (time > timeSpan){
                 Debug.Log("Exiting the app");
                 Application.Quit();
             }
        }
        //reset if the command is aborted
        if(Input.GetKeyUp(KeyCode.Escape)){
            time = 0.0f;
        }
    }
}
