using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RabbitController : MonoBehaviour
{
    public float radius  = 5.0f;
    public float speed = 0.7f;

    public float rotationSpeed = 5f;

    private int IDLE_TIME = 300;
    private int idleCounter = 0;
    private bool isIdle = false;

    private Quaternion lookRotation;

    private Vector3 direction;

    // The point we are going around in circles
    private Vector3 basestartpoint;

    // Destination of our current move
    private Vector3 destination;

    // Start of our current move
    private Vector3 start;

    private Animator animator;

    // Current move's progress
    private float progress = 0.0f;

    private GameObject kinectManager;
    private KinectManager kinectScript;

    private bool shouldAnimate = false;

    private Vector3 interactionPosition;

    // Use this for initialization
    void Start () {

        animator = GetComponent<Animator>();
        start = transform.localPosition;
        basestartpoint = transform.localPosition;
        progress = 0.0f;

        kinectManager = GameObject.Find("KinectManager");
        if(kinectManager != null){
            kinectScript = kinectManager.GetComponent<KinectManager>();
            interactionPosition = kinectScript.GetInteractionPosition();
            interactionPosition = new Vector3(interactionPosition.x, start.y, interactionPosition.z);
        }

        PickNewRandomDestination();
    }

    private bool CheckAnimationTrigger(){
        if(kinectScript != null){
            return kinectScript.CheckTrigger() || Input.GetKeyDown(KeyCode.Alpha1);
        }else{
            return Input.GetKeyDown(KeyCode.Alpha1);
        }
    }

    private void TriggerAnimation(){
        start = transform.localPosition;
        isIdle = true;
        idleCounter = 0;
        IDLE_TIME = Random.Range(200,400);
        progress = 0.0f;
        animator.SetBool("isStanding", true);
        shouldAnimate = false;
    }

    // Update is called once per frame
    void Update () {
        animator.SetBool("isMoving", !isIdle);
        if(CheckAnimationTrigger()){
            shouldAnimate = true;
            isIdle = false;
            destination = interactionPosition;
            direction = destination.normalized;
            lookRotation = Quaternion.LookRotation(-direction);
            progress = 0.0f;
        }


        if(isIdle){
            idleCounter++;
            if(idleCounter >= IDLE_TIME){
                isIdle = false;
                PickNewRandomDestination();
                animator.SetBool("isStanding", false);
            }
            return;
        }

        bool reached = false;
        // Update our progress to our destination
        progress += speed * Time.deltaTime;

        // Check for the case when we overshoot or reach our destination
        if (progress >= 1.0f)
        {
            progress = 1.0f;
            reached = true;
        }

        // Update out position based on our start postion, destination and progress.
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime*rotationSpeed);
        transform.localPosition = (destination * progress) + start * (1 - progress);

        if(reached && !isIdle && shouldAnimate){
            TriggerAnimation();
        }else
        // If we have reached the destination, set it as the new start and pick a new random point. Reset the progress
        if (reached && !isIdle)
        {
            start = destination;
            isIdle = true;
            IDLE_TIME = Random.Range(200,400);
            idleCounter = 0;
            progress = 0.0f;
        }
    }

    void PickNewRandomDestination()
    {
        // We add basestartpoint to the mix so that is doesn't go around a circle in the middle of the scene.
        destination = Random.insideUnitSphere * radius + basestartpoint;
        destination = new Vector3(destination.x, start.y, destination.z);

        direction = destination.normalized;
        lookRotation = Quaternion.LookRotation(-direction);
    }
}
