﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    Vector3 defaultPos;
    public float moveSpeed = 1.0f;
/*
    public GameObject front_camera;
    public GameObject back_camera;
    public GameObject left_camera;
    public GameObject right_camera;
 */

    void Start()
    {
        defaultPos = transform.position;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.U)) // GO UP
        {
            transform.position += Vector3.up * moveSpeed * Time.deltaTime;
        }else if (Input.GetKey(KeyCode.J)) // GO DOWN
        {
            transform.position += -Vector3.up * moveSpeed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.Z)) // PITCH UP
        {
            Vector3 rotateValue = new Vector3(0.1f, 0, 0);
            transform.eulerAngles = transform.eulerAngles - rotateValue;
        }else if (Input.GetKey(KeyCode.H)) // PITCH DOWN
        {
            Vector3 rotateValue = new Vector3(0.1f, 0, 0);
            transform.eulerAngles = transform.eulerAngles + rotateValue;
        }

        /*if (Input.GetKey(KeyCode.Alpha8))
        {
            Debug.Log("up");
            transform.position += Vector3.forward * moveSpeed * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.Alpha2))
        {
            Debug.Log("down");
            transform.position += -Vector3.forward * moveSpeed * Time.deltaTime;
        }else if (Input.GetKey(KeyCode.Alpha4))
        {
            Debug.Log("left");
            transform.position += Vector3.left * moveSpeed * Time.deltaTime;
        }else if (Input.GetKey(KeyCode.Alpha6))
        {
            Debug.Log("right");
            transform.position += -Vector3.left * moveSpeed * Time.deltaTime;
        }*/
            

    }
}
