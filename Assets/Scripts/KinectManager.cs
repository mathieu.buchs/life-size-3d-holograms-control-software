using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Windows.Kinect;
using UnityEngine.UI;

public class KinectManager : MonoBehaviour
{
    private KinectSensor sensor;
    private BodyFrameReader bodyFrameReader;
    private Body[] bodies = null;

    private bool isAvailable;

    private Vector2 wave_leftHand_leftPos = new Vector2(-0.3f, 0.3f); // to check waving movement leftmost position
    private Vector2 wave_leftHand_rightPos = new Vector3(0.3f, 0.3f); // to check waving movement rightmost position

    private bool isLeftPositionReached = false;
    private bool isRightPositionReached = false;


    private bool handDidClose = false;
    private bool handCloseToOpen = false;

    public static KinectManager instance = null;

    public Body[] GetBodies(){
        return bodies;
    }

    void Awake(){
        if(instance == null){
            instance = this;
        }else if(instance != this){
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        sensor = KinectSensor.GetDefault();
        if(sensor != null){
            isAvailable = sensor.IsAvailable;
            bodyFrameReader = sensor.BodyFrameSource.OpenReader();
            if(!sensor.IsOpen){
                sensor.Open();
            }
            bodies = new Body[sensor.BodyFrameSource.BodyCount];
        }
    }

    // Update is called once per frame
    void Update()
    {
        isAvailable = sensor.IsAvailable;
        if(bodyFrameReader != null){
            var frame = bodyFrameReader.AcquireLatestFrame();
            if(frame != null){
                frame.GetAndRefreshBodyData(bodies);
                if(bodies.Length == 0){
                    Debug.Log("Nobody");
                }else{
                    foreach(var body in bodies){
                        /*Windows.Kinect.Joint handLeft = body.Joints[JointType.HandLeft];
                        Windows.Kinect.Joint thumbLeft = body.Joints[JointType.ThumbLeft];
                        Vector2 handLeftV2 = new Vector2(handLeft.Position.X, handLeft.Position.Y);
                        if(handLeftV2 != Vector2.zero)
                            Debug.Log(handLeftV2.ToString());
                        */
   
                        switch (body.HandLeftState){
                            case HandState.Open:
                                handCloseToOpen = handDidClose;
                                break;
                            case HandState.Closed:
                                handDidClose = true;
                                break;
                            default:
                                break;
                        }
   
                        /*if(handLeftV2 == wave_leftHand_leftPos){
                            isLeftPositionReached = true;
                            Debug.Log("Left pos reached");
                        }else if(handLeftV2 == wave_leftHand_rightPos){
                            isRightPositionReached = true;
                            Debug.Log("Right pos reached");
                        }*/



                        /*
                        if(isLeftPositionReached && isRightPositionReached){
                            Debug.Log("HUMAN IS WAVING, DO STH ABOUT IT");
                            isLeftPositionReached = false;
                            isRightPositionReached = false;
                        }*/
                    }
                }
                frame.Dispose();
                frame = null;
            }
        }
    }

    public bool CheckTrigger(){
        if(handDidClose && handCloseToOpen){
            handDidClose = false;
            handCloseToOpen = false;
            return true;
        }
        return false;
    }

    public Vector3 GetInteractionPosition(){
        return transform.position;
    }


    void DrawPoint(Windows.Kinect.Joint joint){
       // joint = joint.ScaleTo(canvas.ActualWidth, canvas.ActualHeight);


    }
}
