﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour {
    Light light;
    Color color;
    bool rsign, gsign, bsign;
    float lightChange = 0.01f;

    // Use this for initialization
    void Start () {
        light = GetComponent<Light>();
        color = new Color(1.0f, 1.0f, 1.0f);
        rsign = true;
        gsign = true;
        bsign = true;
    }

    void checkLightIntensityChange()
    {
        if (Input.GetKey(KeyCode.O))
        {
            light.intensity += lightChange;
        }
        if (Input.GetKey(KeyCode.P))
        {
            light.intensity -= lightChange;
        }
    }

    void checkLightColorChange()
    {
        if (Input.GetKey(KeyCode.R))
        {
            modifyRed();
        }

        if (Input.GetKey(KeyCode.G))
        {
            modifyGreen();
        }

        if (Input.GetKey(KeyCode.B))
        {
            modifyBlue();
        }
        light.color = color;
    }

    void modifyRed()
    {
        if (color.r <= 0.0f)
        {
            rsign = false;
        }
        else if (color.r >= 1.0f)
        {
            rsign = true;
        }

        if (rsign)
        {
            color.r -= lightChange;
        }
        else
        {
            color.r += lightChange;
        }
    }

    void modifyGreen()
    {
        if (color.g <= 0.0f)
        {
            gsign = false;
        }
        else if (color.g >= 1.0f)
        {
            gsign = true;
        }

        if (gsign)
        {
            color.g -= lightChange;
        }
        else
        {
            color.g += lightChange;
        }
    }

    void modifyBlue()
    {
        if (color.b <= 0.0f)
        {
            bsign = false;
        }
        else if (color.b >= 1.0f)
        {
            bsign = true;
        }

        if (bsign)
        {
            color.b -= lightChange;
        }
        else
        {
            color.b += lightChange;
        }
    }
	
	// Update is called once per frame
	void Update () {
        //light on/off
        if (Input.GetKeyDown(KeyCode.V))
        {
            light.enabled = !light.enabled;
        }

        checkLightColorChange();

        checkLightIntensityChange();

        //reset light
        if (Input.GetKeyDown(KeyCode.I))
        {
            light.enabled = true;
            light.intensity = 1.0f;
            color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        }

    }
}
