﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelController : MonoBehaviour {

    //exposed variables
    public float movementSpeed = 180.0f;
    public float scalingValue = 0.1f;
    public float rotationSpeed = 10.0f;

    //private variables
    private Vector3 scaleChange;
    private Vector3 positionChange;

    //components
    private Rigidbody rigidbody;
    private Animator animator;

    private RabbitController rabbitController;


    private void Start()
    {
        animator = GetComponent<Animator>();
        rigidbody = GetComponent<Rigidbody>();
        rabbitController = GetComponent<RabbitController>();
        scaleChange = new Vector3(scalingValue, scalingValue, scalingValue);
        positionChange = new Vector3(0.0f, scalingValue / 2, 0.0f);
    }

    private void FixedUpdate()
    {   //Movement
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        transform.position += new Vector3(moveHorizontal, 0.0f, moveVertical);

        //Rotation
        if (Input.GetKey(KeyCode.Y)) //ROTATE LEFT
        {
            transform.RotateAround(transform.position, transform.up, Time.deltaTime * 90f);
        }
        if (Input.GetKey(KeyCode.C)) //ROTATE RIGHT
        {
            transform.RotateAround(transform.position, transform.up, Time.deltaTime * -90f);
        }

        //Trigger animation
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rigidbody.velocity = Vector3.zero;
            rigidbody.angularVelocity = Vector3.zero;
            //animator.SetTrigger("AnimationTrigger");
            rabbitController.enabled = true;
        }

        if (Input.GetKey(KeyCode.Q)) //scale up
        {
            rigidbody.velocity = Vector3.zero;
            rigidbody.angularVelocity = Vector3.zero;
            transform.localScale += scaleChange;
            transform.position += positionChange;
        }

        if (Input.GetKey(KeyCode.E)) //scale down
        {
            rigidbody.velocity = Vector3.zero;
            rigidbody.angularVelocity = Vector3.zero;
            transform.localScale -= scaleChange;
            transform.position -= positionChange;
        }
    }
}
